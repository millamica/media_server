# Media server in k8s

Self-host your Media Center On Kubernetes with Plex, Sonarr, Radarr, Transmission and Jackett

## Introduction

In the next article of this series, we will learn how to install and configure a Media Center onto our Kubernetes platform to automate the media aggregation and management and play our Media files. The Media Center will be composed of the following components:

- Persistence: A dedicated volume on the SSD to store the data and files
- Torrent Proxy: Jackett is a Torrent Providers Aggregator tool helping to find efficiently BitTorent files over the web
- Downloaders: Transmission is a BitTorrent client to download the files TV Show/Movie Media Management: We'll use Sonarr and Radarr to automate the media aggregation. It searches, launches downloads and renames files when they go out
- Media Center/Player: Plex (server/player) will allow us to make our Media resources accessible from anywhere.

![media server architecture](./readme_media/media_server_k8s_architecture.jpg)

## System

The system we are going to install is going a single node cluster. Our node has a amd-64 cpu with 4 cores and 4GB ram. For the OS we are going to use ubuntu server 20.04 with [microk8s](https://microk8s.io/).

### microk8s config

Before we start we need to ensure that the following [add-ons](https://microk8s.io/docs/addons) are enabled.

Asuming this is a fresh install of microk8s: To install required addons use

```sh
microk8s enable dns ingress storage
```

To check enabled addons use:

```sh
microk8s status
```

## Namespace

We are going to isolate all the Kubernetes objects related to the Media Center into the namespace `media`.

`kubectl create namespace media`

## Ingress

Now going to deploy the ingress responsible of making accessible a service from outside the cluster by mapping an internal service:port to a host. In our case to the node itself (`192.168.1.11` : node's IP). The simplest solution is to use [nip.io](https://nip.io/) which allows us to map an IP (in our case 192.168.1.11) to a hostname without touching `/etc/hosts` or configuring a DNS. Basically it resolves `<anything>.<ip>.nip.io` by `<ip>` without requiring anything else, Magic!

### 1. Create the file ingress.yaml

Create the following Ingress config file `ingress.yaml` to map the routes to each service we will deploy right after this step:

- `http://transmission.192.168.1.11.nip.io/` -`transmission:80`
- `http://sonarr.192.168.1.11.nip.io/` -`sonarr:80`
- `http://jackett.192.168.1.11.nip.io/` -`jackett:80`
- `http://radarr.192.168.1.11.nip.io/` -`radarr:80`
- `http://plex.192.168.1.11.nip.io/` -`plex:80`

### 2. Deploy the ingress

Deploy the Ingress by applying the file `ingress.yaml`.

```sh
kubectl apply -f ingress.yaml
```

### 3. Confirm the Ingress is correctly deployed

Try the URL `http://transmission.192.168.1.11.nip.io` from your browser and confirm it returns the error message `503 Service Temporarily Unavailable` which is normal because we haven't deployed anything yet.

## Storage

Our system is going to store a lot of config and data files that requires persistance. Our system we needs to store configs and special folders for each application (transmission, jackett, sonarr, radarr and plex) also, a shared space for downloads, tvshows and movies.

We are going to store all our data to a external hard drive under `/mnt/disk1`. Then, we need to build the basic directory structure:

- config # where we are going to store config files
  - jackett
  - plex
  - radarr
  - sonarr
  - transmission
- downloads
- movies
- plex # for misc plex data
  - transcode
- transmission # for misc transmission data
  - watch
- tvshows

Now, let's make it into k8s.
K8s in order to claim a local storage directory needs to create a `persistent volume` in wich the system admin sets up directory and space. Each `persistent volume` can be claimed by only one `persitent volume claim`. This way we are going to make a pair of `pv` and `pvc` for each directory / need to store.

Let's start with shared memory: downloads, movies and tvshows
Note that those pv and pvc must have access mode to `ReadWriteMany` in order to be accessible concurrently.

### 2. Deploy the shared volumes

Deploy the Ingress by applying the file `pvc.yaml`.

```sh
kubectl apply -f pvc.yaml
```

The other pv - pvc are going to be explained at given time within each chapter.

## BitTorrent client

The first bit of software to install is [Transmission](https://docs.linuxserver.io/images/docker-transmission), an open-source BitTorent client offering an API, great for integration and automation.

### 1. Create a Kubernetes secret to store your transmission username/password

We first need to safely store our VPN Provider username and password into a [Kubernetes secret](https://kubernetes.io/docs/concepts/configuration/secret/). Replace in the following template `<your-username>` and `<your-password>` using your desired transmission username and password:

```yaml
# transmission_secret.yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: transmission-auth
  namespace: media
type: kubernetes.io/basic-auth
stringData:
  username: <your-username>
  password: <your-password>
```

Deploy the secret by applying the file `transmission_secret.yaml`.

```sh
kubectl apply -f transmission_secret.yaml
```

### 2. Define your config folder

Use the file under `transmission/transmission_data.yaml` and cusomize the local paths on each `PersistentVolume` definition and deploy it.

```sh
kubectl apply -f transmission/transmission_data.yaml
```

### 3. Install the chart bananaspliff/transmission-openvpn

Deploy transmission by applying the file `transmission/transmission.yaml`.

```sh
kubectl apply -f transmission/transmission.yaml
```

### 4. Access to Transmission Web console

Now Transmission and the Nginx Ingress routes are deployed, you should be able to access the Transmission Web console via `http://transmission.192.168.1.11.nip.io/`.

## Torrent Providers Aggregator- Jackett over VPN # TODO

[Jackett](https://github.com/Jackett/Jackett) is a Torrent Providers Aggregator which translates search queries from applications like Sonarr or Radarr into tracker-site-specific http queries, parses the html response, then sends results back to the requesting software. Because some Internet Providers might also block access to Torrent websites, I packaged a version of Jackett using a VPN connection (similar to transmission-over-vpn) accessible on [Docker hub - gjeanmart/jackettvpn:latest-armhf](https://hub.docker.com/repository/docker/gjeanmart/jackettvpn).

### 1. Write the Helm configuration

Let's now configure the chart bananaspliff/jackett. The default configuration can be seen by running the following command helm show values bananaspliff/jackett.

Create the file `media.jackett.values.yml` containing the following configuration.

```yaml
## media.jackett.values.yml
replicaCount: 1

image:
  repository: "gjeanmart/jackettvpn" # Special image to use Jackett over a VPN
  tag: "latest-armhf"
  pullPolicy: IfNotPresent

env:
  - name: VPN_ENABLED
    value: "yes" # Enable Jackett over VPN
  - name: VPN_USERNAME
    valueFrom:
      secretKeyRef: # Reference to the secret | openvpn.username
        name: "openvpn"
        key: "username"
  - name: VPN_PASSWORD
    valueFrom:
      secretKeyRef: # Reference to the secret | openvpn.password
        name: "openvpn"
        key: "password"
  - name: LAN_NETWORK
    value: "192.168.0.0/24"
  - name: CREATE_TUN_DEVICE
    value: "true" # Needed for VPN
  - name: PUID
    value: "1000"
  - name: PGID
    value: "1000"

service:
  type: ClusterIP
  port: 80

volumes:
  - name: "media-hdd"
    persistentVolumeClaim:
      claimName: "media-hdd" # PersistentVolumeClaim created earlier
  - name: "dev-tun" # Needed for VPN
    hostPath:
      path: "/dev/net/tun"

volumeMounts:
  - name: "media-hdd"
    mountPath: "/config"
    subPath: "configs/jackett" # Path /mnt/ssd/media/configs/jackett where jackett writes the configuration
  - name: "media-hdd"
    mountPath: "/downloads"
    subPath: "downloads/jackett" # Path /mnt/ssd/media/downloads/jackett ???

securityContext:
  capabilities: # Needed for VPN
    add:
      - NET_ADMIN
```

### 2. Configure VPN (only if you configured VPN_ENABLED=yes)

a. Create the following directory structure on your SSD

```sh
mkdir -p /mnt/ssd/media/configs/jackett/openvpn/
```

b. Copy one OpenVPN file (usually provided by your VPN provider) into the folder `/mnt/ssd/media/configs/jackett/openvpn/`

c. Create a file credentials.conf into the folder `/mnt/ssd/media/configs/jackett/openvpn/` composed of two line (first one: username and second one password)

```sh
<VPN_USERNAME>
<VPN_PASSWORD>
```

### 3. Pre-configure Jackett

a. Create the following directory structure on your SSD

```sh
mkdir -p /mnt/ssd/media/configs/jackett/Jackett/
```

b. Create the file ServerConfig.json into the folder `/mnt/ssd/media/configs/jackett/Jackett/` with the following content:

```json
{
  "BasePathOverride": "/jackett"
}
```

### 4. Install the chart bananaspliff/jackett

Execute the following command to install the chart `bananaspliff/jackett` with the above configuration onto the namespace `media`.

```sh
helm install jackett bananaspliff/jackett \
    --values media.jackett.values.yml \
    --namespace media
```

After a couple of minutes, you should observe a pod named jackett-xxx Running.

```sh
kubectl get pods -n media -l app=jackett -o wide
```

### 5. Access Jackett

Go to Jackett on `http://media.192.168.1.11.nip.io/jackett` and try to add one or more indexers.

## TV Show Library Management - Sonarr # TODO

[Sonarr](https://sonarr.tv/) is a TV Show library management tool that offers multiple features:

- List all your episodes and see what's missing
- See upcoming episodes
- Automatically search last released episodes (via Jackett) and launch download (via Transmission)
- Move downloaded files into the right directory
- Notify when a new episodes is ready (Kodi, Plex)

### 5.1. Write the Helm configuration

Let's now configure the chart [bananaspliff/sonarr](https://github.com/bananaspliff/geek-charts/tree/gh-pages/sonarr). The default configuration can be seen by running the following command helm show values `bananaspliff/sonarr`.

Create the file media.sonarr.values.yml containing the following configuration.

```yaml
### media.sonarr.values.yml
replicaCount: 1

image:
  repository: linuxserver/sonarr
  tag: arm32v7-latest # ARM image
  pullPolicy: IfNotPresent

env:
  - name: PUID
    value: "1000"
  - name: PGID
    value: "1000"

service:
  type: ClusterIP
  port: 80

volumes:
  - name: media-hdd
    persistentVolumeClaim:
      claimName: "media-hdd" # PersistentVolumeClaim created earlier

volumeMounts:
  - name: media-hdd
    mountPath: "/config"
    subPath: "configs/sonarr" # Path /mnt/ssd/media/configs/sonarr where sonarr writes the configuration
  - name: media-hdd
    mountPath: "/downloads/transmission"
    subPath: "downloads/transmission" # Path /mnt/ssd/media/downloads/transmission where sonarr picks up downloaded episodes
  - name: media-hdd
    mountPath: "/tv"
    subPath: "library/tv" # Path /mnt/ssd/media/library/tv where sonarr moves and renames the episodes
```

### 5.2. Pre-configure Sonarr

a. Create the following directory structure on your SSD

```sh
mkdir -p /mnt/ssd/media/configs/sonarr/
```

b. Create the file config.xml into the folder `/mnt/ssd/media/configs/sonarr/` with the following content:

```xml
<Config>
  <UrlBase>/sonarr</UrlBase>
</Config>
```

### 5.3. Install the chart bananaspliff/sonarr

Execute the following command to install the chart `bananaspliff/sonarr` with the above configuration onto the namespace `media`.

```sh
helm install sonarr bananaspliff/sonarr \
    --values media.sonarr.values.yml \
    --namespace media
```

After a couple of minutes, you should observe a pod named `sonarr-xxx` Running.

```sh
kubectl get pods -n media -l app=sonarr -o wide
```

### 5.4. Access Sonarr

Go to Sonarr on `http://media.192.168.1.11.nip.io/sonarr` and start setting up the library automation. Refer to the wiki for more details.

- Configure the connection to Transmission into **Settings / Download Client / Add (Transmission)** using the hostname and port `transmission-transmission-openvpn.media:80`
- Configure the connection to Jackett into **Settings / Indexers / Add (Torznab / Custom)** using the hostname and port `jackett.media:80`

## Movie Library Management - Radarr

[Radarr](https://radarr.video/) is a Movie library management tool that offers multiple features:

- List all your movies
- Search movies (via Jackett) and launch download (via Transmission)
- Move downloaded files into the right directory
- Notify when a new movie is ready (Kodi, Plex)

### 6.1. Write the Helm configuration

Let's now configure the chart [bananaspliff/radarr](https://github.com/bananaspliff/geek-charts/tree/gh-pages/radarr). The default configuration can be seen by running the following command `helm show values bananaspliff/radarr`.

Create the file `media.radarr.values.yml` containing the following configuration.

```yaml
## media.radarr.values.yml
replicaCount: 1

image:
  repository: "linuxserver/radarr"
  tag: "arm32v7-latest" # ARM image
  pullPolicy: IfNotPresent

env:
  - name: PUID
    value: "1000"
  - name: PGID
    value: "1000"

service:
  type: ClusterIP
  port: 80

volumes:
  - name: "media-hdd"
    persistentVolumeClaim:
      claimName: "media-hdd" # PersistentVolumeClaim created earlier

volumeMounts:
  - name: "media-hdd"
    mountPath: "/config"
    subPath: "configs/radarr" # Path /mnt/ssd/media/configs/radarr where radarr writes the configuration
  - name: "media-hdd"
    mountPath: "/downloads/transmission"
    subPath: "downloads/transmission" # Path /mnt/ssd/media/downloads/transmission where radarr picks up downloaded movies
  - name: media-hdd
    mountPath: "/movies"
    subPath: "library/movies" # Path /mnt/ssd/media/library/movies where radarr moves and renames the movies
```

### 6.2. Pre-configure Radarr

a. Create the following directory structure on your SSD

```sh
mkdir -p /mnt/ssd/media/configs/radarr/
```

b. Create the file config.xml into the folder /mnt/ssd/media/configs/radarr/ with the following content:

```xml
<Config>
  <UrlBase>/radarr</UrlBase>
</Config>
```

### 6.3. Install the chart `bananaspliff/radarr`

Execute the following command to install the chart `bananaspliff/radarr` with the above configuration onto the namespace `media`.

```sh
helm install radarr bananaspliff/radarr \
    --values media.radarr.values.yml \
    --namespace media
```

After a couple of minutes, you should observe a pod named `radarr-xxx` Running.

```sh
kubectl get pods -n media -l app=radarr -o wide
```

### 6.4. Access Radarr # TODO

Go to Radarr on `http://media.192.168.1.11.nip.io/radarr` and start setting up the library automation. Refer to the [wiki](https://github.com/Radarr/Radarr/wiki) for more details.

## Media Server - Plex

[Plex Media Server](https://www.plex.tv/en-gb/) is a software to serve and stream your personal Media library (movies, TV show and music). It fetches the Media resources and builds up a catalogue accessible to any compatible players (Desktop/Mobiles) and transcodes the stream to the player.

In this section, we are going to deploy Plex Media Server (PMS) on Kubernetes using the Helm chart [kube-plex](https://github.com/munnerz/kube-plex).

### 7.1. Clone the charts

This Helm chart is not available via an online repository like jetstack or bananaspliff. We need to download the chart locally. Clone the following repository using `git`.

```sh
git clone https://github.com/munnerz/kube-plex.git
```

### 7.2. Get a claim token

Obtain a Plex Claim Token by visiting [plex.tv/claim](https://plex.tv/claim). You need to create an account if you haven't already one yet.

This will be used to bind your new PMS instance to your own user account automatically.

### 7.3. Create the Helm config file `media.plex.values.yml`

```yaml
## media.plex.values.yml

claimToken: "<CLAIM_TOKEN>" # Replace `<CLAIM_TOKEN>` by the token obtained previously.

image:
  repository: linuxserver/plex
  tag: arm32v7-latest
  pullPolicy: IfNotPresent

kubePlex:
  enabled: false # kubePlex (transcoder job) is disabled because not available on ARM. The transcoding will be performed by the main Plex instance instead of a separate Job.

timezone: Europe/London

service:
  type: LoadBalancer # We will use a LoadBalancer to obtain a virtual IP that can be exposed to Plex Media via our router
  port: 32400 # Port to expose Plex

rbac:
  create: true

nodeSelector: {}

persistence:
  transcode:
    claimName: "media-hdd"
  data:
    claimName: "media-hdd"
  config:
    claimName: "media-hdd"

resources: {}
podAnnotations: {}
proxy:
  enable: false
```

### 7.4. Install Plex using Helm

Now install Plex with Helm specifying our config file media.plex.values.yml and the namespace media:

```sh
helm install plex kube-plex/charts/kube-plex/ \
  --values media.plex.values.yml \
  --namespace media
```

Wait until `kube-plex` (Plex Media Server) is up and running.

```sh
kubectl get pods -n media -l app=kube-plex -o wide
```

You can find the Virtual IP attributed to Plex by MetalLB (in my case `192.168.1.11`).

```sh
kubectl get services -n media -l app=kube-plex -o wide
```

### 7.5. Router config (outside access only)

If you want to access remotely to your Media library, you will need to configure a port-forwarding to allow Plex to access your PMS.

Add a route to port-forward incoming requests on port `32400` to `192.168.1.11:32400` (Plex virtual IP assigned by MetalLB).

### 7.6. Setup Plex

Try now to access (from your network) to Plex Web Player on `http://192.168.1.11:32400`. You should see the setup wizard:

- Click on Got It
- Select Next

You can uncheck Allow me to access my media outside my home if you only want to use Plex within your home network.

- Configure the different Libraries (movies, tv shows, music, etc.)

Our Media will be accessible from the folder `/data/`.

- Click on Done
- All set! Plex will start scrapping your library (bear in mind, this can take a while)
- For outside access, you need to configure the external port used to map outside incoming requests to Plex. Go to Settings / Remote Access and check Manually specify the public to set the port `32400` (as configured in the router - Local)

Notes:

- You can also access Plex from your local network via the ingress: `http://media.192.168.1.11.nip.io/web`
- Download the Android/iOS app and connect to your Plex account, you should automatically see your Plex Media Server with our your Media.

## Summary

Deploy every yaml in the following order:

1. pvc.yaml
2. ingress.yaml
3. foreach: app-name
   - app-name_secret.yaml
   - app-name_data.yaml
   - app-name.yaml
