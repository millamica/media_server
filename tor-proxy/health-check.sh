#!/bin/sh
# check_tor

LOCAL_PROXY='localhost:9050'
CHECK_URL='https://check.torproject.org/api/ip'
ATTEMPTS=2

false; for i in {1..$ATTEMPTS}; do
    if [ ${?} -ne 0 ]; then
        test "$(curl -x socks5h://${LOCAL_PROXY} -s ${CHECK_URL} | jq '.IsTor')" == 'true'
    fi
done